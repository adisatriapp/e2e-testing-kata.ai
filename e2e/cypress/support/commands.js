Cypress.Commands.add("clearFormRegister", () => {
  cy.get(":nth-child(1) > .ant-input").clear(); //name
  cy.get(":nth-child(5) > .ant-input").clear(); //no hp
  cy.get(":nth-child(7) > .ant-input").clear(); //email
  cy.get(".ant-input-affix-wrapper > .ant-input").clear(); //password
});

Cypress.Commands.add("typeLogin", (user) => {
  cy.get("input[name=email]").type(user.email);

  cy.get("input[name=password]").type(user.password);
});

Cypress.Commands.add("filling", (form) => {
  cy.get("#customer_firstname").clear().type(form.customer_firstname);
  cy.get("#customer_lastname").clear().type(form.customer_lastname);
  cy.get("#passwd").clear().type(form.passwd);
  cy.get("#days").select("1");
  cy.get("#months").select("January");
  cy.get("#years").select("2020");
  cy.get("#firstname").clear().type(form.firstname);
  cy.get("#lastname").clear().type(form.lastname);
  cy.get("#company").clear().type(form.company);
  cy.get("#address1").clear().type(form.address1);
  cy.get("#city").clear().type(form.city);
  cy.get("#id_state").clear().type(form.id_state);
  cy.get("#postcode").clear().type(form.postcode);
  cy.get("#id_country").clear().type(form.id_country);
  cy.get("#other").clear().type(form.other);
});

Cypress.Commands.add("filling_form", (data) => {
 
  cy.get("#customer_firstname").clear().type(data.customer_firstname);
  cy.get("#customer_lastname").clear().type(data.customer_lastname);
  cy.get("#passwd").clear().type(data.password);
  cy.get("#days").select("1");
  cy.get("#months").select("January");
  cy.get("#years").select("2020");
  cy.get("#firstname").clear().type(data.firstname);
  cy.get("#lastname").clear().type(data.lastname);
  cy.get("#company").clear().type(data.company);
  cy.get("#address1").clear().type(data.address1);
  cy.get("#city").clear().type(data.city);
  cy.get("#id_country").select(data.id_country);
  cy.get("#id_state").select(data.id_state);
  cy.get("#postcode").clear().type(data.postcode);
  cy.get("#other").clear().type(data.other);
  cy.get('#phone').clear().type(data.phone);
  cy.get('#phone_mobile').clear().type(data.phone_mobile);
  cy.get('#alias').clear().type(data.alias);
  
});
