describe("Login Test", () => {
  before(function () {
    cy.visit("/");
    cy.get(".login").click();
  });
  
  after(function() {
    cy.get('.logout').click()
  });

  beforeEach(function () {
    // "this" points at the test context object
    cy.fixture("user").then((user) => {
      // "this" is still the test context object
      this.user = user;
    });
    cy.fixture("errmass").then((errmass) => {
      this.errmass = errmass;
    });
  });

  it("Submit with empty form", function () {
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.empty_login_form)
    
  });
  it('Valid email and empty password', function () {
    cy.get('#email').clear().type(this.user.registered_email)
    cy.get('#passwd').clear()
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.psswd_login)
  });
  it('Valid email and invalid password', function () {
    cy.get('#email').clear().type(this.user.registered_email)
    cy.get('#passwd').clear().type(this.user.invalid_password)
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.auth_failed)
  });
  it('Empty email and valid password', function () {
    cy.get('#email').clear()
    cy.get('#passwd').clear().type(this.user.password)
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.empty_login_form)
  });
  it('Invalid email and valid password', function () {
    cy.get('#email').clear().type(this.user.invalid_email)
    cy.get('#passwd').clear().type(this.user.password)
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.invalid_email)
  });
  it('unregistered email and valid password', function () {
    cy.get('#email').clear().type(this.user.unregistered)
    cy.get('#passwd').clear().type(this.user.password)
    cy.get('#SubmitLogin').click()
    cy.get('#center_column').should('contain',this.errmass.auth_failed)
  });
  it('valid email and valid password', function () {
    cy.get('#email').clear().type(this.user.registered_email)
    cy.get('#passwd').clear().type(this.user.password)
    cy.get('#SubmitLogin').click()
    cy.url().should('contain','my-account')
    cy.get('.account').should('contain',this.user.full_name)
    cy.get('.info-account').should('contain','Welcome to your account. Here you can manage all of your personal information and orders.')
  });
  
  
});
