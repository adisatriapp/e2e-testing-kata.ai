describe("Register Test", () => {
  before(function () {
    cy.visit("/");
    cy.get(".login").click();
  });

  beforeEach(function () {
    // "this" points at the test context object
    cy.fixture("user").then((user) => {
      // "this" is still the test context object
      this.user = user;
    });

    cy.fixture("errmass").then((errmass) => {
      this.errmass = errmass;
    });
  });

  describe("First form", () => {
    it("Submit with empty email", function () {
      cy.get("#SubmitCreate").click();

      cy.get("#create_account_error").as("errmass_invalid_email");
      cy.get("@errmass_invalid_email").should(
        "contain",
        this.errmass.invalid_email
      );
    });

    it("Ensure the error message disappears when refresh", function () {
      cy.reload();
      cy.get("#create-account_form").should(
        "not.contain",
        this.errmass.invalid_email
      );
    });

    it("Submit with invalid email", function () {
      cy.get("#email_create").clear().type(this.user.invalid_email);
      cy.get("#SubmitCreate").click();
      cy.get("#create_account_error").should(
        "contain",
        this.errmass.invalid_email
      );
    });

    it("Submit with registered email", function () {
      cy.get("#email_create").clear().type(this.user.registered_email);
      cy.get("#SubmitCreate").click();
      cy.get("#create_account_error").should(
        "contain",
        this.errmass.registered_email
      );
    });

    it("Submit with valid email", function () {
      cy.get("#email_create").clear().type(this.user.valid_email);
      cy.get("#SubmitCreate").click();
      cy.url().should("contain", "account-creation");
    });
  });

  describe("Second form", () => {
    it('ensure title "Mr." clickable', () => {
      cy.get('#uniform-id_gender1').click()
        .children('.checked').should('be.visible')
    });
    it('ensure title "Mrs." clickable', () => {
      cy.get('#uniform-id_gender2').click()
        .children('.checked').should('be.visible')
    });
    it('ensure field of "state" and "postal code" appear after choose country', function () {
      cy.get("#id_country").select(this.user.id_country);
      cy.get('.id_state').should('be.visible')
      cy.get('.postcode').should('be.visible')
    });
    it("Submit with empty form", function () {
      cy.get("#submitAccount").click();
      cy.get(".alert")
        .should("contain", this.errmass.phone_numer)
        .should("contain", this.errmass.lastname)
        .should("contain", this.errmass.firstname)
        .should("contain", this.errmass.password)
        .should("contain", this.errmass.address1)
        .should("contain", this.errmass.city)
        .should("contain", this.errmass.postal_code)
        .should("contain", this.errmass.country);
    });

    it('Fill all field without "First name" in personal information', function () {
      cy.filling_form(this.user);
      cy.get("#customer_firstname").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.firstname);
    });
    it('Fill all field without "Last name" in personal information', function () {
      cy.filling_form(this.user);
      cy.get("#customer_lastname").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.lastname);
    });
    it('Fill all field without "Password" in personal information', function () {
      cy.filling_form(this.user);
      cy.get("#passwd").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.password);
    });
    it('Fill all field without "Firstname" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#firstname").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.firstname);
    });
    it('Fill all field without "Lastname" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#lastname").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.lastname);
    });
    it('Fill all field without "Address" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#address1").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.address1);
    });
    it('Fill all field without "City" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#city").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.city);
    });
    it('Fill all field without "Postal code" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#postcode").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.postal_code);
    });
    
    it('Fill all field without "Alias" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#alias").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.alias);
    });
    it('Fill all field without "Mobile Phone" in Address', function () {
      cy.filling_form(this.user);
      cy.get("#phone_mobile").clear();
      cy.get("#submitAccount").click();
      cy.get(".alert").should("contain", this.errmass.phone_numer);
    });
  });
});
